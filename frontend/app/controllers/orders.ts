import Controller from '@ember/controller';
import { action, computed } from '@ember-decorators/object';
import Order from "frontend/models/order";
import CP from '@ember/object/computed';
import DS from 'ember-data';
import { inject as service } from '@ember-decorators/service';
import { task } from "ember-concurrency-decorators";
import { timeout } from "ember-concurrency";

export default class Orders extends Controller {
  selectedOrder = null;
  search = '';

  @service store!: CP<DS.Store>;

  @task({ restartable: true })
  * searchOrders(filter: string) {
    yield timeout(500);
    let orders = yield this.store.query('order', { q: { number_or_email_cont: filter } });
    this.set('filteredOrders', orders);
  }

  @action
  printReceipt() {
    window.print();
  }

  @action
  async selectOrder(order: Order) {
    this.set('selectedOrder', await this.store.findRecord('order', order.number));
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'orders': Orders;
  }
}
