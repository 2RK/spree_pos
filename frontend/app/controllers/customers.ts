import Controller from '@ember/controller';
import { action } from '@ember-decorators/object';
import CP from '@ember/object/computed';
import DS from 'ember-data';
import { inject as service } from '@ember-decorators/service';
import { task } from "ember-concurrency-decorators";
import { timeout } from "ember-concurrency";
import User from "frontend/models/user";

export default class Customers extends Controller {
  selectedCustomer = null;
  search = '';

  @service store!: CP<DS.Store>;

  @task({ restartable: true })
  * searchCustomers(filter: string) {
    yield timeout(500);
    let users = yield this.store.query('user', { q: { email_or_bill_address_firstname_or_bill_address_lastname_cont: filter } });
    this.set('filteredCustomers', users);
  }

  @action
  async selectCustomer(customer: User) {
    this.set('selectedCustomer', customer);
    this.set('selectedCustomer.orders', await this.store.query('order', { q: { user_id_eq: customer.id } }));
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'customers': Customers;
  }
}
