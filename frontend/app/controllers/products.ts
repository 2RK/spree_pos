import Controller from '@ember/controller';
import { task } from "ember-concurrency-decorators";
import { timeout } from "ember-concurrency";
import Product from "frontend/models/product";
import { inject as service } from '@ember-decorators/service';
import CP from "@ember/object/computed";
import DS from "ember-data";



export default class Products extends Controller {
  search = '';
  showPaymentModal = false;
  showCustomerModal = false;

  @service cookies;
  @service store!: CP<DS.Store>;

  @task({ drop: true })
  * finaliseOrder() {
    let order = this.get('model.order');
    const cookieService = this.get('cookies');
    order.set('state', 'complete');
    order.set('paymentState', 'paid');
    order.set('completedAt', new Date);
    yield order.save();
    let newOrder = yield this.get('store').createRecord('order').save();
    this.set('model.order', newOrder);
    cookieService.write('orderNumber', newOrder.number);
  }

  @task({ restartable: true })
  * searchProducts(filter: string) {
    yield timeout(500);
    let products = yield this.store.query('product', { q: { name_or_description_or_master_sku_cont: filter } });
    this.set('filteredProducts', products);
  }

  @task({ drop: true })
  * clearCart() {
    yield this.get('model.order').clear();
    this.set('model.order', yield this.get('model.order').reload());
  }

  @task({ drop: true })
  * addToCart(product: Product) {
    let order = this.get('model.order');
    let lineItem = this.get('model.order.lineItems').find((lineItem) => lineItem.variant_id.toString() === product.master.id.toString());
    if (lineItem) {
      lineItem.set('quantity', lineItem.quantity + 1);
    } else {
      lineItem = this.get('store').createRecord('line-item', {
        variant_id: product.master.id,
        quantity: 1,
        order: order,
      });
    }
    yield lineItem.save();
    this.set('model.order', yield this.get('model.order').reload());
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'products': Products;
  }
}
