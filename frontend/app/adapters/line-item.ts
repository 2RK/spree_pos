import Application from "frontend/adapters/application";
import { RequestData } from "frontend/adapters/order";

export default class LineItem extends Application {
  urlForCreateRecord(modelName, snapshot) {
    return `/shop/api/v1/orders/${snapshot._internalModel._record.get('order.number')}/line_items`
  }

  urlForUpdateRecord(id, modelName, snapshot) {
    return `/shop/api/v1/orders/${snapshot._internalModel._record.get('order.number')}/line_items/${snapshot.id}`
  }

  urlForDeleteRecord(id, modelName, snapshot) {
    return `/shop/api/v1/orders/${snapshot._internalModel._record.get('order.number')}/line_items/${snapshot.id}`
  }

  createRecord(store, type, snapshot) {
    let data = {};
    const serializer = store.serializerFor(type.modelName);

    serializer.serializeIntoHash(data, type, snapshot, { includeId: true });

    const id = snapshot.id;
    const url = this.buildURL(type.modelName, id, snapshot, 'createRecord');

    data.line_item = data.lineItem;

    return this.ajax(url, 'POST', { data: data });
  }

  updateRecord(store, type, snapshot) {
    let data = {};
    const serializer = store.serializerFor(type.modelName);

    serializer.serializeIntoHash(data, type, snapshot, { includeId: true });

    const id = snapshot.id;
    const url = this.buildURL(type.modelName, id, snapshot, 'updateRecord');

    data.line_item = data.lineItem;

    return this.ajax(url, 'PUT', { data: data });
  }

  handleResponse(status: number, headers: any, payload: any, requestData: RequestData) {
    if (!payload || payload.lineItems) {
      return super.handleResponse(status, headers, payload, requestData);
    } else {
      return super.handleResponse(status, headers, { lineItems: payload }, requestData);
    }
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your adapters.
declare module 'ember-data/types/registries/adapter' {
  export default interface AdapterRegistry {
    'line-item': LineItem;
  }
}
