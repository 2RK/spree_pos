import Application from "frontend/adapters/application";

export default class PaymentMethod extends Application {
  urlForFindAll(query, modelName) {
    return `/shop/api/v1/payment_methods`
  }

  urlForQuery(query, modelName) {
    return `/shop/api/v1/payment_methods`
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your adapters.
declare module 'ember-data/types/registries/adapter' {
  export default interface AdapterRegistry {
    'payment-method': PaymentMethod;
  }
}
