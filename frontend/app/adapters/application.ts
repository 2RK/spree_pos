import DS from 'ember-data';

export default class Application extends DS.RESTAdapter {
  host = 'http://localhost:3000/shop';
  namespace = 'api/v1';

  init() {
    this._super(...arguments);

    this.set('headers', {
      'X-Spree-Token': 'abc-123'
    });
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your serializers.
declare module 'ember-data/types/registries/adapter' {
  export default interface AdapterRegistry {
    'application': Application;
  }
}
