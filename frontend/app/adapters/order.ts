import Application from "frontend/adapters/application";

export default class Order extends Application {
  buildURL(modelName, id, snapshot, requestType, query)  {
    if (requestType == 'capturePayment') {
      return `/shop/api/v1/orders/${id}/payments/${snapshot._attributes.paymentId}/capture`;
    } else {
      return super.buildURL(modelName, id, snapshot, requestType, query);
    }
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your adapters.
declare module 'ember-data/types/registries/adapter' {
  export default interface AdapterRegistry {
    'order': Order;
  }
}
