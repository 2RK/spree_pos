import ApplicationSerializer from "frontend/serializers/application";
import DS from "ember-data";
import { isPresent } from "@ember/utils";

const AddressSerializer = ApplicationSerializer.extend(DS.EmbeddedRecordsMixin, {
  serialize(snapshot: DS.Snapshot) {
    let json = snapshot.attributes();

    if (isPresent(json.country)) {
      json.country_id = json.country.id;
      json.country = null;
    }

    if (isPresent(json.state)) {
      json.state_id = json.state.id;
      json.state = null;
    }

    return json;
  }
});

export default AddressSerializer

// DO NOT DELETE: this is how TypeScript knows how to look up your serializers.
declare module 'ember-data/types/registries/serializer' {
  export default interface SerializerRegistry {
    'address': AddressSerializer;
  }
}
