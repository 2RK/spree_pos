import ApplicationSerializer from "frontend/serializers/application";
import DS from "ember-data";

const UserSerializer = ApplicationSerializer.extend(DS.EmbeddedRecordsMixin, {
  attrs: {
    billAddress: { embedded: 'always' },
    shipAddress: { embedded: 'always' }
  },
});

export default UserSerializer

// DO NOT DELETE: this is how TypeScript knows how to look up your serializers.
declare module 'ember-data/types/registries/serializer' {
  export default interface SerializerRegistry {
    'user': UserSerializer;
  }
}
