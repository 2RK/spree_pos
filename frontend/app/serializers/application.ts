import DS from 'ember-data';
import { underscore } from '@ember/string';

const Application = DS.RESTSerializer.extend({
  keyForAttribute(attr) {
    return underscore(attr);
  }
});

export default Application;

// DO NOT DELETE: this is how TypeScript knows how to look up your serializers.
declare module 'ember-data/types/registries/serializer' {
  export default interface SerializerRegistry {
    'application': Application;
  }
}
