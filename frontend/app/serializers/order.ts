import ApplicationSerializer from "frontend/serializers/application";
import DS from "ember-data";
import { underscore } from "@ember/string";


const OrderSerializer = ApplicationSerializer.extend(DS.EmbeddedRecordsMixin, {
  primaryKey: 'number',
  attrs: {
    lineItems: { embedded: 'always' },
    billAddress: { embedded: 'always' },
    shipAddress: { embedded: 'always' }
  },

  serialize(snapshot: DS.Snapshot) {
    let json = {};
    if (snapshot.id === null) {
      json = { line_items: [] }
    } else {
      json = snapshot._attributes;
      Object.keys(json).map((key, index) => {
        return json[underscore(key)] = json[key];
      });

      json.line_items = null;
      json.payments = null;
    }

    return json;
  }
});

export default OrderSerializer

// DO NOT DELETE: this is how TypeScript knows how to look up your serializers.
declare module 'ember-data/types/registries/serializer' {
  export default interface SerializerRegistry {
    'application': ApplicationSerializer;
  }
}
