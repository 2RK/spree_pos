import ApplicationSerializer from "frontend/serializers/application";

const ProductSerializer = ApplicationSerializer.extend({
  extractMeta(store, typeClass, payload) {
    if (payload) {
      let meta = {
        count: payload.count,
        current_page: payload.current_page,
        total_count: payload.total_count,
        per_page: payload.per_page,
        pages: payload.page,

      };
      return meta;
    }
  }
});

export default ProductSerializer

// DO NOT DELETE: this is how TypeScript knows how to look up your serializers.
declare module 'ember-data/types/registries/serializer' {
  export default interface SerializerRegistry {
    'product': ProductSerializer;
  }
}
