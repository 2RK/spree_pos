/*
|-------------------------------------------------------------------------------
| Colors                                    https://tailwindcss.com/docs/colors
|-------------------------------------------------------------------------------
|
| Here you can specify the colors used in your project. To get you started,
| we've provided a generous palette of great looking colors that are perfect
| for prototyping, but don't hesitate to change them for your project. You
| own these colors, nothing will break if you change everything about them.
|
| We've used literal color names ("red", "blue", etc.) for the default
| palette, but if you'd rather use functional names like "primary" and
| "secondary", or even a numeric scale like "100" and "200", go for it.
|
*/

export default {
  'transparent': 'transparent',

  'black': '#000000',
  'grey-1': '#1F2933',
  'grey-2': '#323F4B',
  'grey-3': '#3E4C59',
  'grey-4': '#52606D',
  'grey-5': '#616E7C',
  'grey-6': '#7B8794',
  'grey-7': '#9AA5B1',
  'grey-8': '#CBD2D9',
  'grey-9': '#E4E7EB',
  'grey-10': '#F5F7FA',
  'white': '#ffffff',

  'blue-1': '#002159',
  'blue-2': '#01337D',
  'blue-3': '#03449E',
  'blue-4': '#0552B5',
  'blue-5': '#0967D2',
  'blue-6': '#2186EB',
  'blue-7': '#47A3F3',
  'blue-8': '#7CC4FA',
  'blue-9': '#BAE3FF',
  'blue-10': '#E6F6FF',

  'cyan-1': '#05606E',
  'cyan-2': '#07818F',
  'cyan-3': '#099AA4',
  'cyan-4': '#0FB5BA',
  'cyan-5': '#1CD4D4',
  'cyan-6': '#3AE7E1',
  'cyan-7': '#62F4EB',
  'cyan-8': '#92FDF2',
  'cyan-9': '#C1FEF6',
  'cyan-10': '#E1FCF8',

  'orange-1': '#610316',
  'orange-2': '#8A041A',
  'orange-3': '#AB091E',
  'orange-4': '#CF1124',
  'orange-5': '#E12D39',
  'orange-6': '#EF4E4E',
  'orange-7': '#F86A6A',
  'orange-8': '#FF9B9B',
  'orange-9': '#FFBDBD',
  'orange-10': '#FFE3E3',

  'red-1': '#610316',
  'red-2': '#8A041A',
  'red-3': '#AB091E',
  'red-4': '#CF1124',
  'red-5': '#E12D39',
  'red-6': '#EF4E4E',
  'red-7': '#F86A6A',
  'red-8': '#FF9B9B',
  'red-9': '#FFBDBD',
  'red-10': '#FFE3E3',

  'yellow-1': '#8D2B0B',
  'yellow-2': '#B44D12',
  'yellow-3': '#CB6E17',
  'yellow-4': '#DE911D',
  'yellow-5': '#F0B429',
  'yellow-6': '#F7C948',
  'yellow-7': '#FADB5F',
  'yellow-8': '#FCE588',
  'yellow-9': '#FFF3C4',
  'yellow-10': '#FFFBEA',
};
