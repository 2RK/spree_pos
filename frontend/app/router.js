import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('products', { path: '/shop/pos' });
  this.route('customers', { path: '/shop/pos/customers' });
  this.route('orders', { path: '/shop/pos/orders' });
});

export default Router;
