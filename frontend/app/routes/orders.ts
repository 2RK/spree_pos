import Route from '@ember/routing/route';

export default class Orders extends Route {
  model() {
    return this.store.findAll('order', { include: 'products' });
  }

  async setupController(controller, model) {
    super.setupController(controller, model);
    controller.set('filteredOrders', model);
    controller.set('selectedOrder', await this.store.queryRecord('order', { id: model.get('firstObject.number') }));
  }
}
