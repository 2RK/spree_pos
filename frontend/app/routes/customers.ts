import Route from '@ember/routing/route';
import RSVP from "rsvp";

export default class Customers extends Route {
  model() {
    return RSVP.hash({
      users: this.store.findAll('user'),
      countries: this.store.findAll('country'),
      states: this.store.findAll('state'),
    });
  }

  async setupController(controller, model) {
    super.setupController(controller, model);
    controller.set('filteredCustomers', model.users);
    controller.set('selectedCustomer', model.users.get('firstObject'));
    controller.set('selectedCustomer.orders', await this.store.query('order', { q: { user_id_eq: model.users.get('firstObject.id') } }));
  }
}
