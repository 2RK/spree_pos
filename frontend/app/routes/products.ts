import Route from '@ember/routing/route';
import RSVP from 'rsvp';
import { inject as service } from '@ember-decorators/service';

export default class Products extends Route {
  @service cookies;
  @service infinity;

  model() {
    const cookieService = this.get('cookies');
    const orderNumber = cookieService.read('orderNumber');
    let order;
    if (orderNumber) {
      order = this.store.findRecord('order', orderNumber);
    } else {
      order = this.store.createRecord('order').save();
    }

    return RSVP.hash({
      products: this.infinity.model('product', {
        perPage: 20,
        perPageParam: 'per_page',
        pageParam: 'current_page',
        totalPagesParam: 'meta.pages',
        countParam: 'meta.total_count',
      }),
      order,
      users: this.store.findAll('user'),
      paymentMethods: this.store.query('payment-method', { q: { display_on_in: ['both', 'backend'] } }),
    });
  }

  setupController(controller, model) {
    super.setupController(controller, model);
    controller.set('filteredProducts', model.products);
    controller.set('selectedCustomer', model.users.find((user) => user.id.toString() === model.order.userId.toString()));
    const cookieService = this.get('cookies');
    cookieService.write('orderNumber', model.order.number);
  }
}
