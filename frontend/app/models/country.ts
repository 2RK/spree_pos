import Model from 'ember-data/model';
import { attr } from '@ember-decorators/data';

export default class Country extends Model {
  @attr('string') name!: string;
}

// DO NOT DELETE: this is how TypeScript knows how to look up your models.
declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    'country': Country;
  }
}
