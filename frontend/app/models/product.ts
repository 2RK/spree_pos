import Model from 'ember-data/model';
import { attr } from '@ember-decorators/data';

export type Variant = {
  name: string,
  total_on_hand: number,
  images: [{
    product_url: string,
  }]
}

export default class Product extends Model {
  @attr('string') name!: string;
  @attr('string') description!: string;
  @attr({ defaultValue: () => { {} } }) master!: Variant;
}

// DO NOT DELETE: this is how TypeScript knows how to look up your models.
declare module 'ember-data' {
  interface modelRegistry {
    'product': Product;
  }
}
