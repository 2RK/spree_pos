import Model from 'ember-data/model';
import { attr, hasMany, belongsTo } from '@ember-decorators/data';
import date from "ember-data/transforms/date";
import Address, { Country } from "frontend/models/address";
import LineItem from "frontend/models/line-item";
import { memberAction } from "ember-api-actions";
import User from "frontend/models/user";
import { sum, mapBy } from '@ember-decorators/object/computed';

export type Payment = {
  amount: number;
}

export default class Order extends Model {
  @hasMany lineItems!: LineItem;
  @belongsTo('address', { inverse: null }) billAddress!: Address;
  @belongsTo('address', { inverse: null }) shipAddress!: Address;
  @belongsTo('user', { inverse: null }) user!: User;

  @attr('string') email!: string;
  @attr('string') number!: string;
  @attr('string') shipmentState!: string;
  @attr('string') state!: string;
  @attr('string') paymentState!: string;
  @attr('date') createdAt!: date;
  @attr('date') completedAt!: date;
  @attr('number') total!: number;
  @attr('number') taxTotal!: number;
  @attr('number') itemTotal!: number;
  @attr('number') userId!: number;
  @attr('number') paymentId!: number;
  @attr({ defaultValue: () => [] }) payments!: Payment[];

  @mapBy('payments', 'amount') paymentAmounts;
  @sum('paymentAmounts') totalPaid!: number;

  clear = memberAction({ path: 'empty' });
  payment = memberAction({ path: 'payments', ajaxOptions: { method: 'POST' } });
  capture = memberAction({ urlType: 'capturePayment', ajaxOptions: { method: 'PUT' } });
}

// DO NOT DELETE: this is how TypeScript knows how to look up your models.
declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    'order': Order;
  }
}
