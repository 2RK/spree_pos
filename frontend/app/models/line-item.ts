import Model from 'ember-data/model';
import { attr, belongsTo } from '@ember-decorators/data';
import Order from "frontend/models/order";

export type Variant = {
  name: string,
  images: [{
    mini_url: string,
  }]
}

export default class LineItem extends Model {
  @belongsTo('order') order!: Order;

  @attr('string') variant_id!: string;
  @attr('number') quantity!: number;
  @attr('number') price!: number;
  @attr('number') total!: number;
  @attr({ defaultValue: () => { {} } }) variant!: Variant;
}

// DO NOT DELETE: this is how TypeScript knows how to look up your models.
declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    'line-item': LineItem;
  }
}
