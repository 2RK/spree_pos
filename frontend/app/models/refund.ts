import Model from 'ember-data/model';
import { attr, hasMany, belongsTo } from '@ember-decorators/data';
import date from "ember-data/transforms/date";
import Address from "frontend/models/address";
import LineItem from "frontend/models/line-item";
import { memberAction } from "ember-api-actions";
import Order from "frontend/models/order";

export default class Refund extends Model {
  @belongsTo('order', { inverse: null }) order!: Order;

  @attr('string') amount!: string;
  @attr('number') refundReasonId!: number;
  @attr('string') number!: string;
}

// DO NOT DELETE: this is how TypeScript knows how to look up your models.
declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    'refund': Refund;
  }
}
