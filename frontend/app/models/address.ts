import Model from 'ember-data/model';
import { attr, hasMany } from '@ember-decorators/data';
import Order from "frontend/models/order";

export type Country = {
  name: string
}

export type State = {
  name: string
}

export default class Address extends Model {
  @hasMany orders!: Order;

  @attr('string') email!: string;
  @attr('string') fullName!: string;
  @attr('string') firstname!: string;
  @attr('string') lastname!: string;
  @attr('string') phone!: string;
  @attr('string') address1!: string;
  @attr('string') zipcode!: string;
  @attr('string') city!: string;
  @attr({ defaultValue: () => { {} } }) country!: Country;
  @attr({ defaultValue: () => { {} } }) state!: State;
}

// DO NOT DELETE: this is how TypeScript knows how to look up your models.
declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    'address': Address;
  }
}
