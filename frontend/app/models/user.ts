import Model from 'ember-data/model';
import { attr, belongsTo } from '@ember-decorators/data';
import Address from "frontend/models/address";
import date from "ember-data/transforms/date";

export default class User extends Model {
  @belongsTo('address', { inverse: null }) billAddress!: Address;
  @belongsTo('address', { inverse: null }) shipAddress!: Address;

  @attr('string') email!: string;
  @attr('date') createdAt!: date;
  @attr('string') posCustomer!: boolean;
  @attr('number') billAddressId!: number;
  @attr('number') shipAddressId!: number;
}

// DO NOT DELETE: this is how TypeScript knows how to look up your models.
declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    'user': User;
  }
}
