import Component from '@ember/component';
import { task } from "ember-concurrency-decorators";
import { inject as service } from '@ember-decorators/service';
import CP from "@ember/object/computed";
import DS from "ember-data";
import { action } from '@ember-decorators/object';
import PaymentMethod from 'frontend/models/payment-method';

export default class PaymentForm extends Component {
  @service cookies;
  @service store!: CP<DS.Store>;
  showModal = false;

  @action
  showModal() {
    this.set('showModal', true);
    this.set('currentPaymentMethod', null);
    this.set('paymentAmount', this.get('order.total'));
    this.set('changeRequired', 0);
  }

  @task({ drop: true })
  * submitPayment() {
    let order = this.get('order');
    
    if (this.get('paymentAmount') > this.get('order.total')) {
      this.set('changeRequired', this.get('paymentAmount') - this.get('order.total'));
      this.set('paymentAmount', this.get('order.total'));
    }
    
    yield order.payment({
      payment: {
        payment_method_id: this.get('currentPaymentMethod.id'),
        amount: this.get('paymentAmount'),
        state: 'completed'
      }
    });

    this.set('order', yield this.get('order').reload());
    if (this.get('changeRequired') === 0) {
      this.set('showModal', false);
    }
    this.set('paymentMethod', '');
  }

  @action
  setPaymentMethod(paymentMethod: PaymentMethod) {
    this.set('currentPaymentMethod', paymentMethod);
    this.set('defaultAmount', true);
  }

  @action
  resetToTotal() {
    this.set('paymentAmount', this.get('order.total'));
    this.set('defaultAmount', true);
  }

  @action
  addToTotal(cashAmount: number) {
    if (this.get('defaultAmount')) {
      this.set('paymentAmount', 0);
    }
    this.incrementProperty('paymentAmount', cashAmount);
    this.set('defaultAmount', false);
  }
};
