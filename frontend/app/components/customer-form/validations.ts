import {
  validatePresence,
} from 'ember-changeset-validations/validators';

export default {
  email: validatePresence(true),
  firstname: validatePresence(true),
  lastname: validatePresence(true),
  phone: validatePresence(true),

  billAddress1: validatePresence(true),
  billCity: validatePresence(true),
  billState: validatePresence(true),
  billCountry: validatePresence(true),
  billZipcode: validatePresence(true),

  shipAddress1: validatePresence(true),
  shipCity: validatePresence(true),
  shipState: validatePresence(true),
  shipCountry: validatePresence(true),
  shipZipcode: validatePresence(true)
};
