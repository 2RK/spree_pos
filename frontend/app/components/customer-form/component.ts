import Component from '@ember/component';
import { action } from '@ember-decorators/object';
import Changeset from 'ember-changeset';
import lookupValidator from 'ember-changeset-validations';
import User from 'frontend/models/customer';
import CP from '@ember/object/computed';
import DS from 'ember-data';
import { inject as service } from '@ember-decorators/service';
import validations from './validations';
import { isBlank } from "@ember/utils";

export default class CustomerForm extends Component {
  customer!: User;
  editCustomer?: boolean;
  changeset?: Changeset;

  isShowingModal = false;

  @service store!: CP<DS.Store>;

  async createChangeset() {
    this.set('customer', this.editCustomer ? this.customer : this.get('store').createRecord('user'));
    this.set('changeset', new Changeset(this.customer, lookupValidator(validations), validations));
    this.setBillingAddress();
    this.setShippingAddress();
  }

  async setBillingAddress() {
    if (isBlank(await this.get('customer.billAddress'))) {
      this.set('customer.billAddress', this.get('store').createRecord('address'));
    }

    this.get('changeset').setProperties({
      firstname: this.get('customer.billAddress.firstname'),
      lastname: this.get('customer.billAddress.lastname'),
      phone: this.get('customer.billAddress.phone'),
      billAddress1: this.get('customer.billAddress.address1'),
      billAddress2: this.get('customer.billAddress.address2'),
      billCity: this.get('customer.billAddress.city'),
      billCountry: this.get('customer.billAddress.country'),
      billZipcode: this.get('customer.billAddress.zipcode'),
      billState: this.get('customer.billAddress.state'),
    });
  }

  async setShippingAddress() {
    if (isBlank(await this.get('customer.shipAddress'))) {
      this.set('customer.shipAddress', this.get('store').createRecord('address'));
    }

    this.get('changeset').setProperties({
      shipAddress1: this.get('customer.shipAddress.address1'),
      shipAddress2: this.get('customer.shipAddress.address2'),
      shipCity: this.get('customer.shipAddress.city'),
      shipCountry: this.get('customer.shipAddress.country'),
      shipZipcode: this.get('customer.shipAddress.zipcode'),
      shipState: this.get('customer.shipAddress.state'),
    });
  }

  @action
  toggleModal() {
    this.createChangeset();
    this.toggleProperty('isShowingModal');
  }

  @action
  closeModal() {
    this.set('isShowingModal', false);
  }

  @action
  async submit(changeset: Changeset) {
    await changeset.validate();

    if (changeset.get('isValid')) {
      await this.saveShippingAddress(changeset);
      await this.saveBillingAddress(changeset);
      await this.saveCustomer(changeset);

      this.set('isShowingModal', false);
    }
  }

  async saveCustomer(changeset: Changeset) {
    let customer = this.get('customer');
    customer.setProperties({
      email: changeset.get('email'),
      posCustomer: true,
      billAddressId: this.get('customer.billAddress.id'),
      shipAddressId: this.get('customer.shipAddress.id'),
    });
    await customer.save();
  }

  async saveBillingAddress(changeset: Changeset) {
    let billAddress = await this.get('customer.billAddress');

    billAddress.setProperties({
      fullName: `${changeset.get('firstname')} ${changeset.get('lastname')}`,
      firstname: changeset.get('firstname'),
      lastname: changeset.get('lastname'),
      phone: changeset.get('phone'),
      address1: changeset.get('billAddress1'),
      address2: changeset.get('billAddress2'),
      city: changeset.get('billCity'),
      country: changeset.get('billCountry'),
      zipcode: changeset.get('billZipcode'),
      state: changeset.get('billState'),
    });

    await billAddress.save();
  }

  async saveShippingAddress(changeset: Changeset) {
    let shipAddress = await this.get('customer.shipAddress');

    shipAddress.setProperties({
      fullName: `${changeset.get('firstname')} ${changeset.get('lastname')}`,
      firstname: changeset.get('firstname'),
      lastname: changeset.get('lastname'),
      phone: changeset.get('phone'),
      address1: changeset.get('shipAddress1'),
      address2: changeset.get('shipAddress2'),
      city: changeset.get('shipCity'),
      country: changeset.get('shipCountry'),
      zipcode: changeset.get('shipZipcode'),
      state: changeset.get('shipState'),
    });

    await shipAddress.save();
  }

  @action
  cancel() {
    this.changeset.rollback();
    this.toggleProperty('isShowingModal');
  }
};
