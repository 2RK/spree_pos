import Component from '@ember/component';
import { task } from "ember-concurrency-decorators";
import CP from "@ember/object/computed";
import DS from "ember-data";
import { inject as service } from '@ember-decorators/service';

export default class ReturnForm extends Component {
  @service store!: CP<DS.Store>;

  @task({ drop: true })
  * returnItems() {
    let order = yield this.get('order');
    yield this.get('store').createRecord('refund', {
      refundReasonId: 1,
      amount: order.total,
      number: order.id,
    }).save();
  }
};
