import Component from '@ember/component';
import { tagName } from '@ember-decorators/component';

@tagName('')
class AppNav extends Component {
  // normal class body definition here
};

export default AppNav;
