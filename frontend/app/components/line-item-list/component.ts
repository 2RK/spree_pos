import Component from '@ember/component';
import { task } from "ember-concurrency-decorators";

export default class LineItemList extends Component {
  @task({ drop: true })
  * updateQuantity() {
    if (this.get('lineItem.quantity') === '0') {
      yield this.get('lineItem').destroyRecord();
    } else {
      yield this.get('lineItem').save();
    }
    this.set('showModal', false);
  }
};
