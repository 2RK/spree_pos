import Component from '@ember/component';
import { task } from "ember-concurrency-decorators";
import { timeout } from "ember-concurrency";
import User from "frontend/models/user";
import CP from "@ember/object/computed";
import DS from "ember-data";
import { action } from '@ember-decorators/object';
import { inject as service } from '@ember-decorators/service';

export default class CustomerSelect extends Component {
  @service store!: CP<DS.Store>;

  @task({ restartable: true })
  * searchCustomers(filter: string) {
    yield timeout(500);
    let users = yield this.store.query('user', { q: { email_or_bill_address_firstname_or_bill_address_lastname_cont: filter } });
    this.set('filteredCustomers', users);
  }

  @action
  async selectCustomer(customer: User) {
    let order = this.get('order');
    order.set('userId', customer.id);
    await order.save();
    this.set('selectedCustomer', customer);
    await order.reload();
    this.set('showModal', false);
  }
};
