Spree::Core::Engine.add_routes do
  mount_ember_app :frontend, to: "/pos", controller: '/spree_pos/ember'

  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      resources :addresses, only: [:create, :update], controller: '/spree_pos/api/v1/addresses'
      get '/payment_methods' => '/spree_pos/api/v1/payment_methods#index'
      resources :refunds, only: [:create], controller: '/spree_pos/api/v1/refunds'
    end
  end
end

