Spree::PermittedAttributes.user_attributes << :pos_customer
Spree::PermittedAttributes.user_attributes << :ship_address_id
Spree::PermittedAttributes.user_attributes << :bill_address_id
Spree::PermittedAttributes.payment_attributes << :state
Spree::PermittedAttributes.checkout_attributes << :payment_state
Spree::PermittedAttributes.checkout_attributes << :state
Spree::PermittedAttributes.checkout_attributes << :completed_at

