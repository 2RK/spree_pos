module Spree
  User.class_eval do
    attr_accessor :pos_customer

    def password_required?
      if pos_customer
        false
      else
        super
      end
    end
  end
end
