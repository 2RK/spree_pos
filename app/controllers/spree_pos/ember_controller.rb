module SpreePos
  class EmberController < BaseController
    unless ancestors.include? ActionController::Base
      (
      ActionController::Base::MODULES -
        ActionController::API::MODULES
      ).each do |controller_module|
        include controller_module
      end

      helper EmberRailsHelper
    end

    def index
      if current_spree_user && current_spree_user.spree_roles.include?(Spree::Role.find_by(name: 'pos'))
        render layout: false
      else
        redirect_to root_path
      end
    end
  end
end
