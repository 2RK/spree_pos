module SpreePos
  class BaseController < Spree::BaseController
    helper 'spree/admin/navigation'
    layout '/spree/layouts/admin'
  end
end
