class SpreePos::Api::V1::AddressesController < Spree::Api::BaseController
  def create
    authorize! :create, @address
    @address = Spree::Address.new(address_params)

    if @address.save
      respond_with(@address, status: 201, default_template: :show)
    else
      invalid_resource!(@address)
    end
  end

  def update
    @address = Spree::Address.find_by!(id: params[:id])
    authorize! :update, @address

    if @address.update_attributes(address_params)
      respond_with(@address, status: 200, default_template: :show)
    else
      invalid_resource!(@address)
    end
  end

  private

  def permitted_address_attributes
    super - [:country, :state] + [:country_id, :state_id]
  end

  def address_params
    params.require(:address).permit(permitted_address_attributes)
  end
end
