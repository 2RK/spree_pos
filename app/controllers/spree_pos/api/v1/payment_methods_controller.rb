module SpreePos
  module Api
    module V1
      class PaymentMethodsController < Spree::Api::BaseController
        def index
          @payment_methods = Spree::PaymentMethod.ransack(params[:q]).result
          @payment_methods = @payment_methods.page(params[:page]).per(params[:per_page])

          respond_with(@payment_methods)
        end
      end
    end
  end
end
