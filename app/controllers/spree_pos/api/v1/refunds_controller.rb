module SpreePos
  module Api
    module V1
      class RefundsController < Spree::Api::BaseController
        def create
          @order = Spree::Order.find_by(number: params[:refund][:number])
          @refund = Spree::Refund.new(refund_params.merge(payment_id: @order.payment_ids.first))
          authorize! :create, @refund

          if @refund.save
            respond_with(@refund, default_template: :show)
          else
            invalid_resource!(@refund)
          end
        end

        private

        def refund_params
          params.require(:refund).permit(:amount, :refund_reason_id)
        end
      end
    end
  end
end
