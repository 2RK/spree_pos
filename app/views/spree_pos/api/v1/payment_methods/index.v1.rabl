object false
child(@payment_methods => :payment_methods) do
  attributes *payment_method_attributes
end
node(:count) { @payment_methods.count }
node(:current_page) { params[:page].try(:to_i) || 1 }
node(:pages) { @payment_methods.total_pages }
